package ru.tsc.vinokurov.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.tsc.vinokurov.tm.api.repository.ICommandRepository;
import ru.tsc.vinokurov.tm.api.service.ICommandService;
import ru.tsc.vinokurov.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @Override
    public Collection<AbstractCommand> getArgumentCommands() {
        return commandRepository.getArgumentCommands();
    }

    @Override
    public void add(AbstractCommand command) {
        commandRepository.add(command);
    }


    @Override
    public AbstractCommand getCommandByName(String name) {
        if (StringUtils.isEmpty(name)) return null;
        return commandRepository.getCommandByName(name);
    }

    @Override
    public AbstractCommand getCommandByArgument(String argument) {
        if (StringUtils.isEmpty(argument)) return null;
        return commandRepository.getCommandByArgument(argument);
    }

}
