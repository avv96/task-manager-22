package ru.tsc.vinokurov.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.tsc.vinokurov.tm.api.repository.IProjectRepository;
import ru.tsc.vinokurov.tm.api.repository.ITaskRepository;
import ru.tsc.vinokurov.tm.api.repository.IUserRepository;
import ru.tsc.vinokurov.tm.api.service.IUserService;
import ru.tsc.vinokurov.tm.enumerated.Role;
import ru.tsc.vinokurov.tm.exception.entity.UserExistsByEmailException;
import ru.tsc.vinokurov.tm.exception.entity.UserExistsByLoginException;
import ru.tsc.vinokurov.tm.exception.entity.UserNotFoundException;
import ru.tsc.vinokurov.tm.exception.field.*;
import ru.tsc.vinokurov.tm.model.User;
import ru.tsc.vinokurov.tm.util.HashUtil;

import java.util.Optional;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    private ITaskRepository taskRepository;

    private IProjectRepository projectRepository;

    public UserService(
            final IUserRepository userRepository,
            final ITaskRepository taskRepository,
            final IProjectRepository projectRepository
    ) {
        super(userRepository);
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public User findOneById(final String id) {
        if (StringUtils.isEmpty(id)) throw new UserIdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    public User findOneByLogin(final String login) {
        if (StringUtils.isEmpty(login)) throw new LoginEmptyException();
        return repository.findOneByLogin(login);
    }

    @Override
    public User remove(final User user) {
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        final User userToRemove = super.remove(user);
        if (userToRemove == null) return null;
        final String userId = userToRemove.getId();
        taskRepository.clear(userId);
        projectRepository.clear(userId);
        return userToRemove;
    }

    @Override
    public User findOneByEmail(final String email) {
        if (StringUtils.isEmpty(email)) throw new EmailEmptyException();
        return repository.findOneByEmail(email);
    }

    @Override
    public User removeByLogin(final String login) {
        if (StringUtils.isEmpty(login)) throw new EmailEmptyException();
        final User user = Optional.ofNullable(findOneByLogin(login)).orElseThrow(UserNotFoundException::new);
        return remove(user);
    }

    @Override
    public User create(final String login, final String password) {
        if (StringUtils.isEmpty(login)) throw new LoginEmptyException();
        if (StringUtils.isEmpty(password)) throw new PasswordEmptyException();
        if (repository.existsByLogin(login)) throw new UserExistsByLoginException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt((password)));
        return repository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (StringUtils.isEmpty(email)) throw new EmailEmptyException();
        if (repository.existsByEmail(email)) throw new UserExistsByEmailException();
        final User user = Optional.ofNullable(create(login, password)).orElseThrow(UserNotFoundException::new);
        user.setEmail(email);
        return repository.add(user);
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (role == null) throw new RoleEmptyException();
        final User user = Optional.ofNullable(create(login, password)).orElseThrow(UserNotFoundException::new);
        user.setRole(role);
        return repository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email, final Role role) {
        Optional.ofNullable(role).orElseThrow(RoleEmptyException::new);
        final User user = Optional.ofNullable(create(login, password, email)).orElseThrow(UserNotFoundException::new);
        user.setRole(role);
        return repository.add(user);
    }

    @Override
    public User setPassword(final String userId, final String password) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(password)) throw new PasswordEmptyException();
        final User user = Optional.ofNullable(findOneById(userId)).orElseThrow(UserNotFoundException::new);
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User updateUser(
            final String userId, final String firstName,
            final String lastName, final String middleName
    ) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        final User user = Optional.ofNullable(findOneById(userId)).orElseThrow(UserNotFoundException::new);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public void lockUserByLogin(final String login) {
        if (StringUtils.isEmpty(login)) throw new LoginEmptyException();
        final User user = Optional.ofNullable(findOneByLogin(login)).orElseThrow(UserNotFoundException::new);
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(final String login) {
        if (StringUtils.isEmpty(login)) throw new LoginEmptyException();
        final User user = Optional.ofNullable(findOneByLogin(login)).orElseThrow(UserNotFoundException::new);
        user.setLocked(false);
    }
}
