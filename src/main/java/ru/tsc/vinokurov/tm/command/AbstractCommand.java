package ru.tsc.vinokurov.tm.command;

import org.apache.commons.lang3.StringUtils;
import ru.tsc.vinokurov.tm.api.model.ICommand;
import ru.tsc.vinokurov.tm.api.service.IAuthService;
import ru.tsc.vinokurov.tm.api.service.IServiceLocator;
import ru.tsc.vinokurov.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract String getName();

    public abstract String getDescription();

    public abstract String getArgument();

    public abstract void execute();

    public abstract Role[] getRoles();

    protected IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public String getUserId() {
        return getAuthService().getUserId();
    }

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        final String name = getName();
        final String description = getDescription();
        final String argument = getArgument();
        String result = "";
        if (!StringUtils.isEmpty(name)) result += name + " : ";
        if (!StringUtils.isEmpty(argument)) result += argument + " : ";
        if (!StringUtils.isEmpty(description)) result += description;
        return result;
    }

}
