package ru.tsc.vinokurov.tm.exception.field;

public final class DateBeginEmptyException extends AbstractFieldException {

    public DateBeginEmptyException() {
        super("Error! Date Begin is empty...");
    }

}
