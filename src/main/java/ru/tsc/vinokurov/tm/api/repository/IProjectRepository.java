package ru.tsc.vinokurov.tm.api.repository;

import ru.tsc.vinokurov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    Project create(String userId, String name);

    Project create(String userId, String name, String description);

    boolean existsById(String userId, String id);

}
