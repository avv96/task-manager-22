package ru.tsc.vinokurov.tm.api.service;

import ru.tsc.vinokurov.tm.api.repository.IRepository;
import ru.tsc.vinokurov.tm.enumerated.Sort;
import ru.tsc.vinokurov.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    M removeById(String id);

    M removeByIndex(Integer index);

    List<M> findAll(Sort sort);

}
